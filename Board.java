public class Board {
  private Die dieOne;
  private Die dieTwo;
  private int[][] closedTiles;
  final int max = 3;
  
  public Board() {
    this.dieOne = new Die();
    this.dieTwo = new Die();
    this.closedTiles = new int[6][6];
  }
  
  public String toString() { 
    String s = "";
    for (int i=0; i < closedTiles.length;i++){
      for (int j=0; j < closedTiles[i].length; j++){
        s = s + closedTiles[i][j]; 
      }
      s = s + "  ";
    }
    return s;
  } 
  
  public boolean playATurn() {
    dieOne.roll();
    dieTwo.roll();
    System.out.println(dieOne);
    System.out.println(dieTwo);
    int p1 = dieOne.getPips() - 1;
    int p2 = dieTwo.getPips() - 1;
    System.out.println("Position: "+p1);
    System.out.println("Position: "+p2);
    closedTiles[p1][p2] = (closedTiles[p1][p2])+1;
    System.out.println(closedTiles[p1][p2]);
    if(closedTiles[p1][p2] < max){
      System.out.println("The tile at this position is smaller than 3.");
      return true;
    }
    System.out.println("The tile at this position is bigger than 3.");
    return false;
    }
}


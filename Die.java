import java.util.Random;
public class Die {
  private int pips;
  private Random r;
  
public Die () {
  this.r = new Random();
  this.pips=1; 
}

public int getPips() {
  return this.pips;
}

public Random getRandom() {
  return this.r;
}
public void roll() {
  int ran = (this.r.nextInt(6)+1);
  this.pips=ran;
}
public String toString() {
  return "There are " +this.pips+ " pips. ";
}
}
